import React, {useDebugValue, useState} from 'react';
import {ViewBase} from 'react-native';
import {
  View,
  Text,
  TouchableOpacity,
  TextInput,
  StyleSheet,
  ScrollView,
  Button,
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import {useDispatch, useSelector} from 'react-redux';
import DateTimePicker from '@react-native-community/datetimepicker';

const AddDataHome = ({navigation, route}) => {
  const [storeName, setStoreName] = useState('');
  const [address, setAddress] = useState('');
  const [idStore, setIdStore] = useState('');
  const [availableTime, setAvailableTime] = useState(null);
  const [closedTime, setClosedTime] = useState(null);
  const [description, setDescription] = useState('');
  const [favourite, setFavourite] = useState(false);
  const [isOpen, setIsOpen] = useState('');
  const [minimumPrice, setMinimumPrice] = useState('');
  const [maximumPrice, setMaximumPrice] = useState('');
  const [ratings, setRatings] = useState('');
  const [storeImage, setStoreImage] = useState('');

  const {homeData} = useSelector(state => state.home);
  const dispatch = useDispatch();

  console.log('HOME DATA', homeData);
  const storeData = () => {
    var date = new Date();
    var dataHome = [...homeData];

    const data = {
      id: date.getMilliseconds(),
      address: address,

      idStore: idStore,
      availableTime: availableTime,
      closedTime: closedTime,
      description: description,
      favourite: favourite,
      isOpen: isOpen,
      minimumPrice: minimumPrice,
      maximumPrice: maximumPrice,
      ratings: ratings,
      storeImage:
        // storeImage
        'http://aduma.tokyo/wp-content/uploads/2014/08/140519_502-2-s.jpg',
      storeName: storeName,
    };
    dataHome.push(data);
    dispatch({type: 'ADD_DATA', data: dataHome});
    navigation.goBack();
  };

  const updateData = () => {
    //buat update data
    console.log('ubah', homeData);
    const data = {
      id: route.params.item.id,
      address: address,

      idStore: idStore,
      availableTime: availableTime,
      closedTime: closedTime,
      description: description,
      favourite: favourite,
      isOpen: isOpen,
      minimumPrice: minimumPrice,
      maximumPrice: maximumPrice,
      ratings: ratings,
      storeImage: 
      'http://aduma.tokyo/wp-content/uploads/2014/08/140519_502-2-s.jpg',
      storeName: storeName,
    };
    dispatch({type: 'UPDATE_DATA', data});
    navigation.goBack();
  };

  const deleteData = async () => {
    dispatch({type: 'DELETE_DATA', id: route.params.item.id});
    navigation.goBack();
  };

  const checkData = () => {
    //fungsi cek data
    if (route.params) {
      const data = route.params.item;
      setAddress(data.address);

      setIdStore(data.idStore);
      setAvailableTime(data.availableTime);
      setClosedTime(data.closedTime);
      setDescription(data.description);
      setFavourite(data.favourite);
      setIsOpen(data.isOpen);
      setMinimumPrice(data.minimumPrice);
      setMaximumPrice(data.maximumPrice);
      setRatings(data.ratings);
      setStoreImage(data.storeImage);
      setStoreName(data.storeName);
    }
  };

  React.useEffect(() => {
    checkData();
  }, []);

  const [showBukaPicker, setShowBukaPicker] = useState(false);
  const [showTutupPicker, setShowTutupPicker] = useState(false);

  const showBukaTimePicker = () => {
    setShowBukaPicker(true);
  };

  const showTutupTimePicker = () => {
    setShowTutupPicker(true);
  };

  const handleBukaTimeChange = (event, selectedTime) => {
    setShowBukaPicker(false);
    if (selectedTime) {
      const timeString = selectedTime.toLocaleTimeString('en-US', {
        hour: 'numeric',
        minute: 'numeric',
        hour12: false,
      });
      setAvailableTime(timeString);
    }
  };

  const handleTutupTimeChange = (event, selectedTime) => {
    setShowTutupPicker(false);
    if (selectedTime) {
      const timeString = selectedTime.toLocaleTimeString('en-US', {
        hour: 'numeric',
        minute: 'numeric',
        hour12: false,
      });
      setClosedTime(timeString);
    }
  };

  return (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      <ScrollView //component yang digunakan agar tampilan kita bisa discroll
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{paddingBottom: 10}}>
        <View style={styles.header}>
          <TouchableOpacity
            style={{
              width: '15%',
              justifyContent: 'center',
              alignItems: 'center',
            }}
            onPress={() => navigation.goBack()}>
            <Icon name="arrowleft" size={25} color="#fff" />
          </TouchableOpacity>
          <View
            style={{
              width: '70%',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text style={{fontSize: 16, fontWeight: '600', color: '#fff'}}>
              {route.params ? 'Ubah Data' : 'Tambah Data'}
            </Text>
          </View>
          <View style={{width: '15%'}} />
        </View>
        <View
          style={{
            flex: 1,
            padding: 15,
          }}>
          <TextInput
            placeholder="Masukkan nama Store"
            style={styles.txtInput}
            value={storeName}
            onChangeText={text => setStoreName(text)}
          />
          <TextInput
            placeholder="Masukkan Alamat"
            style={[
              styles.txtInput,
              {
                height: 90,
                textAlignVertical: 'top',
              },
            ]}
            multiline
            value={address}
            onChangeText={text => setAddress(text)}
          />

          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-evenly',
              marginTop: 20,
            }}>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Text>Buka</Text>
              <TouchableOpacity
                onPress={() => setIsOpen('Buka')}
                style={{
                  height: 25,
                  width: 25,
                  borderWidth: 1,
                  borderRadius: 15,
                  marginLeft: 10,
                  backgroundColor: isOpen == 'Buka' ? 'green' : 'white',
                }}>
                {isOpen == 'Buka' ? (
                  <Icon name="check" size={20} color="white" />
                ) : null}
              </TouchableOpacity>
            </View>

            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Text>Tutup</Text>
              <TouchableOpacity
                onPress={() => setIsOpen('Tutup')}
                style={{
                  height: 25,
                  width: 25,
                  borderWidth: 1,
                  borderRadius: 15,
                  marginLeft: 10,
                  backgroundColor: isOpen == 'Tutup' ? 'red' : 'white',
                }}>
                {isOpen == 'Tutup' ? (
                  <Icon name="check" size={20} color="white" />
                ) : null}
              </TouchableOpacity>
            </View>
          </View>

          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <View>
              <Text style={{color: 'black'}}>Jam Buka</Text>
              <TouchableOpacity
                onPress={showBukaTimePicker}
                style={styles.txtDatePicker}>
                <Text style={{margin: 10, color: 'black'}}>
                  {availableTime ? availableTime : 'Pilih jam'}
                </Text>
              </TouchableOpacity>
              {showBukaPicker && (
                <DateTimePicker
                  value={new Date()}
                  mode="time"
                  is24Hour={true}
                  display="spinner"
                  onChange={handleBukaTimeChange}
                />
              )}
            </View>
            <View>
              <Text style={{color: 'black'}}>Jam Tutup</Text>
              <TouchableOpacity
                onPress={showTutupTimePicker}
                style={styles.txtDatePicker}>
                <Text style={{margin: 10, color: 'black'}}>
                  {closedTime ? closedTime : 'Pilih jam'}
                </Text>
              </TouchableOpacity>
              {showTutupPicker && (
                <DateTimePicker
                  value={new Date()}
                  mode="time"
                  is24Hour={true}
                  display="spinner"
                  onChange={handleTutupTimeChange}
                />
              )}
            </View>
          </View>

          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <TextInput
              placeholder="Harga minimal"
              style={styles.txtInputPrice}
              value={minimumPrice}
              onChangeText={text => setMinimumPrice(text)}
            />
            <TextInput
              placeholder="Harga maksimal"
              style={styles.txtInputPrice}
              value={maximumPrice}
              onChangeText={text => setMaximumPrice(text)}
            />
          </View>
          <TextInput
            placeholder="Masukkan jumlah rating"
            style={styles.txtInput}
            value={ratings}
            onChangeText={text => setRatings(text)}
          />

          <TextInput
            placeholder="Masukkan Deskripsi"
            style={[
              styles.txtInput,
              {
                height: 90,
                textAlignVertical: 'top',
              },
            ]}
            multiline
            value={description}
            onChangeText={text => setDescription(text)}
          />
          <TouchableOpacity
            style={styles.btnAdd}
            onPress={() => {
              if (route.params) {
                updateData();
              } else {
                storeData();
              }
            }}>
            <Text style={{fontSize: 14, color: '#fff', fontWeight: '600'}}>
              {route.params ? 'Ubah' : 'Tambah'}
            </Text>
          </TouchableOpacity>

          {route.params && (
            <TouchableOpacity
              style={[styles.btnAdd, {backgroundColor: '#dd2c00'}]}
              onPress={deleteData}>
              <Text style={{fontSize: 14, color: '#fff', fontWeight: '600'}}>
                Hapus
              </Text>
            </TouchableOpacity>
          )}
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  btnAdd: {
    marginTop: 20,
    width: '100%',
    backgroundColor: '#43a047',
    borderRadius: 6,
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 15,
  },
  txtInput: {
    width: '100%',
    borderRadius: 6,
    borderColor: '#dedede',
    borderWidth: 1,
    paddingHorizontal: 10,
    marginTop: 20,
  },
  txtDatePicker: {
    width: '100%',
    borderRadius: 6,
    borderColor: '#dedede',
    borderWidth: 1,
    paddingHorizontal: 10,
    marginTop: 20,
  },
  txtInputPrice: {
    width: '45%',
    borderRadius: 6,
    borderColor: '#dedede',
    borderWidth: 1,
    paddingHorizontal: 10,
    marginTop: 20,
  },
  header: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#3f51b5',
    paddingVertical: 15,
  },
});

export default AddDataHome;
