import React, { useEffect } from 'react';
import {
    View,
    Text,
    ScrollView,
    KeyboardAvoidingView,
    Image,
    TouchableOpacity,
    FlatList,
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { styles } from './Style';
import Icon from 'react-native-vector-icons/AntDesign';
import { Rating } from './Rating';

const Home = ({ navigation, route }) => {
    const { homeData } = useSelector(state => state.home);
    console.log('homedata', homeData);
    const dispatch = useDispatch();

    const handleFavoritePress = (item) => {
        //buat update data
        console.log('ubah', homeData);
        const data = {
            ...item,
          favourite: !item.favourite,
        };
        dispatch({type: 'UPDATE_DATA', data});
      };

      useEffect(() => {
      }, []);

    return (
        <View style={styles.container}>
            <ScrollView //component yang digunakan agar tampilan kita bisa discroll
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{ paddingBottom: 10 }}>
                <View style={styles.header}>
                    <View style={styles.photoBag}>
                        <TouchableOpacity
                            onPress={() => navigation.navigate('DatePickers')}
                        >
                            <Image
                                source={require('../assets/image/profile_photo.png')} //load asset dari local
                                style={{
                                    width: 45,
                                    height: 45,
                                    resizeMode: 'contain',
                                }}
                            />
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => navigation.navigate('Keranjangs')}
                        >
                            <Image
                                source={require('../assets/icon/Bag_ic.png')} //load asset dari local
                                style={{
                                    width: 24,
                                    height: 24,
                                    resizeMode: 'contain',
                                }}
                            />
                        </TouchableOpacity>
                    </View>

                    <View style={styles.textHeader}>
                        <Text
                            style={{
                                fontSize: 15,
                                color: '#034262',
                                fontFamily: 'Montserrat',
                                fontStyle: 'normal',
                                fontWeight: '500',
                                marginVertical: 5,
                            }}>
                            Hello Fahri!
                        </Text>
                        <Text
                            style={{
                                fontSize: 20,
                                color: '#0A0827',
                                fontFamily: 'Montserrat',
                                fontStyle: 'normal',
                                fontWeight: '700',
                            }}>
                            Ingin merawat dan perbaiki sepatumu? cari disini
                        </Text>
                    </View>

                    <View style={styles.searchFilter}>
                        <View
                            style={{
                                justifyContent: 'center',
                                paddingLeft: 10,
                                width: 275,
                                height: 45,
                                backgroundColor: '#F6F8FF',
                                borderRadius: 13,
                            }}>
                            <Image
                                source={require('../assets/icon/Search_ic.png')} //load asset dari local
                                style={{
                                    width: 24,
                                    height: 24,
                                    resizeMode: 'contain',
                                }}
                            />
                        </View>
                        <View
                            style={{
                                alignItems: 'center',
                                justifyContent: 'center',
                                width: 45,
                                height: 45,
                                backgroundColor: '#F6F8FF',
                                borderRadius: 13,
                            }}>
                            <Image
                                source={require('../assets/icon/Filter_ic.png')} //load asset dari local
                                style={{
                                    width: 24,
                                    height: 24,
                                    resizeMode: 'contain',
                                }}
                            />
                        </View>
                    </View>
                </View>
                <View style={styles.categoryBox}>
                    <View style={styles.categoryItems}>
                        <TouchableOpacity>
                            <Image
                                source={require('../assets/icon/Sepatu_ic.png')} //load asset dari local
                                style={{
                                    width: 45,
                                    height: 45,
                                    resizeMode: 'contain',
                                }}
                            />
                            <Text
                                style={{
                                    fontSize: 9,
                                    color: '#BB2427',
                                    fontWeight: '600',
                                    textAlign: 'center',
                                    marginTop: 10,
                                }}>
                                Sepatu
                            </Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.categoryItems}>
                        <TouchableOpacity>
                            <Image
                                source={require('../assets/icon/Jaket_ic.png')} //load asset dari local
                                style={{
                                    width: 45,
                                    height: 45,
                                    resizeMode: 'contain',
                                }}
                            />
                            <Text
                                style={{
                                    fontSize: 9,
                                    color: '#BB2427',
                                    fontWeight: '600',
                                    textAlign: 'center',
                                    marginTop: 10,
                                }}>
                                Jaket
                            </Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.categoryItems}>
                        <TouchableOpacity
                        >
                            <Image
                                source={require('../assets/icon/Tas_ic.png')} //load asset dari local
                                style={{
                                    width: 45,
                                    height: 45,
                                    resizeMode: 'contain',
                                }}
                            />
                            <Text
                                style={{
                                    fontSize: 9,
                                    color: '#BB2427',
                                    fontWeight: '600',
                                    textAlign: 'center',
                                    marginTop: 10,
                                }}>
                                Tas
                            </Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={styles.recomendBox}>
                    <View
                        style={{
                            justifyContent: 'space-between',
                            flexDirection: 'row',
                            alignItems: 'center',
                        }}>
                        <View style={styles.recomendText}>
                            <Text
                                style={{
                                    fontSize: 16,
                                    color: '#0A0827',
                                    fontWeight: '600',
                                    marginRight: 75,
                                }}>
                                Rekomendasi Terdekat
                            </Text>
                            <Text
                                style={{
                                    fontSize: 14,
                                    color: '#E64C3C',
                                    fontWeight: '500',
                                    marginLeft: 75,
                                }}>
                                View All
                            </Text>
                        </View>
                    </View>
                    <View style={styles.boxList}>
                        <FlatList
                            data={homeData}
                            keyExtractor={(item, index) => index.toString()}
                            renderItem={({ item, index }) => (

                                <View style={styles.recomendItems}>
                                    <TouchableOpacity
                                        onPress={() => navigation.navigate('AddDataHome', { item })}
                                    >
                                        <Image
                                            source={{ uri: item.storeImage }}
                                            style={{
                                                borderRadius: 5,
                                                width: 80,
                                                height: 121,
                                                resizeMode: 'contain',
                                            }}
                                        />
                                    </TouchableOpacity>
                                    <View style={{ marginHorizontal: 20 }}>

                                        <View style={styles.starheart}>
                                            <View style={styles.stars}>
                                                <Rating
                                                    rating={item.ratings}
                                                    colorOn='#FFC107'
                                                    colorOff='#ADADAD'
                                                    iconSize={12} />

                                            </View>
                                            <TouchableOpacity onPress={() => handleFavoritePress(item)}>
                                                <Icon
                                                    // name={item.favorite ? 'hearto' : 'heart'}
                                                    name={'heart'}
                                                    size={16}
                                                    color={item.favourite ? '#F94343' : '#ADADAD'}
                                                    style={{marginLeft: 170}}
                                                />
                                            </TouchableOpacity>

                                        </View>
                                        <Text
                                            style={{
                                                fontSize: 10,
                                                color: '#B2B2B2',
                                                fontWeight: '500',
                                                marginLeft: 2,
                                            }}>
                                            {item.ratings} ratings
                                        </Text>
                                        <Text
                                            style={{
                                                fontSize: 12,
                                                color: '#201F26',
                                                fontWeight: '600',
                                                marginLeft: 2,
                                                paddingVertical: 7,
                                            }}>
                                            {item.storeName}
                                        </Text>
                                        <Text
                                            style={{
                                                fontSize: 9,
                                                color: '#B2B2B2',
                                                fontWeight: '500',
                                                marginLeft: 2,
                                                paddingBottom: 3,
                                            }}>
                                            {item.address}
                                        </Text>
                                        <TouchableOpacity onPress={() => navigation.navigate('DetailScreen', item)}>
                                            <View
                                                style={{
                                                    width: 55,
                                                    height: 21,
                                                    borderRadius: 10.5,
                                                    backgroundColor: item.isOpen == 'Buka' ? 'rgba(17, 168, 78, 0.12)' : 'rgba(230, 76, 60, 0.2)',
                                                    alignItems: 'center',
                                                    justifyContent: 'center',
                                                    marginVertical: 5,
                                                }}>
                                                <Text style={{
                                                    fontSize: 12,
                                                    color: item.isOpen == 'Buka' ? '#11A84E' : '#EA3D3D',
                                                    fontWeight: '700',
                                                }}>{item.isOpen}</Text>
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            )}

                        />
                    </View>


                </View>
            </ScrollView>
            <TouchableOpacity
                activeOpacity={0.8}
                style={styles.btnFloating}
                onPress={() => navigation.navigate('AddDataHome')}>
                <Icon name="plus" size={25} color="#fff" />
            </TouchableOpacity>
        </View>
    );
};
export default Home;
