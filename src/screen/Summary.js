import React from 'react';
import {
  View,
  Text,
  TextInput,
  ScrollView,
  Image,
  TouchableOpacity,
} from 'react-native';
import {styles} from './Style';

const Summary = ({navigation, route}) => {
  return (
    <View style={styles.container}>
      <View
        style={{
          width: '100%',
          height: 60,
          backgroundColor: '#FFFFFF',
          flexDirection: 'row',
          alignSelf: 'center',
          alignItems: 'center',
          paddingLeft: 14,
          shadowColor: '#000',
          shadowOffset: {
            width: 0,
            height: 1,
          },
          shadowOpacity: 0.22,
          shadowRadius: 2.22,

          elevation: 3,
        }}>
        <TouchableOpacity onPress={() => navigation.navigate('Keranjangs')}>
          <Image
          style={{
            size: 24,
            color: '#BB2427',
          }}
          source={require('../assets/icon/blackBack_ic.png')}
          />
        </TouchableOpacity>
        <Text
          style={{
            fontFamily: 'Montserrat',
            marginLeft: 14,
            fontWeight: '700',
            fontSize: 18,
            lineHeight: 17,
          }}>
          Summary
        </Text>
      </View>
      <ScrollView //component yang digunakan agar tampilan kita bisa discroll
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{paddingBottom: 10}}>
        <View
          style={{
            width: '100%',
            height: 152,
            backgroundColor: '#FFFFFF',
            alignSelf: 'center',
            flexDirection: 'column',
            marginVertical: 10,
          }}>
          <Text
            style={{
              paddingTop: 10,
              marginLeft: 14,
              fontFamily: 'Monsserrat',
              fontWeight: '500',
              fontSize: 14,
              lineHeight: 17,
              color: '#979797',
            }}>
            Data Customer
          </Text>
          <Text
            style={{
              fontFamily: 'Monsserrat',
              fontWeight: '400',
              fontSize: 14,
              lineHeight: 17,
              color: '#000000',
              marginLeft: 14,
              paddingTop: 10,
            }}>
            Fahrii Fahriza (081357088476)
          </Text>
          <Text
            style={{
              fontFamily: 'Monsserrat',
              fontWeight: '400',
              fontSize: 14,
              lineHeight: 17,
              color: '#000000',
              marginLeft: 14,
              paddingTop: 6,
            }}>
            Taman Lucida No. 5, Warungboto, Umbulharjo, Yogyakarta
          </Text>
          <Text
            style={{
              fontFamily: 'Monsserrat',
              fontWeight: '400',
              fontSize: 14,
              lineHeight: 17,
              color: '#000000',
              marginLeft: 14,
              paddingTop: 6,
            }}>
            fahriifahriza@gmail.com
          </Text>
        </View>
        <View
          style={{
            width: '100%',
            height: 107,
            backgroundColor: '#FFFFFF',
            alignSelf: 'center',
            flexDirection: 'column',
            backgroundColor: '#FFFFFF',
            marginVertical: 10,
          }}>
          <Text
            style={{
              paddingTop: 10,
              marginLeft: 14,
              fontFamily: 'Monsserrat',
              fontWeight: '500',
              fontSize: 14,
              lineHeight: 17,
              color: '#979797',
            }}>
            Alamat Outlet Tujuan
          </Text>
          <Text
            style={{
              fontFamily: 'Monsserrat',
              fontWeight: '400',
              fontSize: 14,
              lineHeight: 17,
              color: '#000000',
              marginLeft: 14,
              paddingTop: 10,
            }}>
            Jack-Repair - Seturan (0983243248)
          </Text>
          <Text
            style={{
              fontFamily: 'Monsserrat',
              fontWeight: '400',
              fontSize: 14,
              lineHeight: 17,
              color: '#000000',
              marginLeft: 14,
              paddingTop: 6,
            }}>
            Jalan Corongan Maguwoharjo Depok
          </Text>
        </View>

        <View
          style={{
            width: '100%',
            height: 172,
            backgroundColor: '#FFFFFF',
            alignSelf: 'center',
            flexDirection: 'column',
            backgroundColor: '#FFFFFF',
            marginVertical: 10,
          }}>
          <Text
            style={{
              paddingTop: 10,
              marginLeft: 14,
              fontFamily: 'Monsserrat',
              fontWeight: '500',
              fontSize: 14,
              lineHeight: 17,
              color: '#979797',
            }}>
            Barang
          </Text>
          <View
            style={{
              width: '87%',
              height: 133,
              backgroundColor: '#FFFFFF',
              alignSelf: 'flex-start',
              marginLeft: 14,
              flexDirection: 'row',
              marginVertical: 5,
            }}>
            <Image
              style={{alignSelf: 'center',}}
              source={require('../assets/image/sample_sepatu.png')}></Image>
            <View
              style={{
                flexDirection: 'column',
                alignSelf: 'center',
                paddingLeft: 13,
              }}>
              <Text
                style={{
                  fontFamily: 'Monsserrat',
                  fontWeight: '500',
                  fontSize: 12,
                  lineHeight: 15,
                  color: '#000000',
                }}>
                New Balance - Pink Abu - 40
              </Text>
              <Text
                style={{
                  fontFamily: 'Monsserrat',
                  fontWeight: '400',
                  fontSize: 12,
                  lineHeight: 15,
                  color: '#737373',
                  paddingTop: 11,
                }}>
                Cuci sepatu
              </Text>
              <Text style={{paddingTop: 10}}>Note : -</Text>
            </View>
          </View>
        </View>

        <View style={styles.cartButton}>
          <TouchableOpacity onPress={() => navigation.navigate('Checkout')}>
            <Text
              style={{
                fontSize: 16.5,
                color: '#FFFFFF',
                fontWeight: '700',
              }}>
              Reservasi Sekarang
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};
export default Summary;
