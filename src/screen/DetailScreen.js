import React from 'react';
import {
  Image,
  Text,
  TouchableOpacity,
  View,
  ScrollView,
  ImageBackground,
} from 'react-native';
import {styles} from './Style';
import {Rating} from './Rating';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

const DetailScreen = ({navigation, route}) => {
  console.log('data detail', route.params);
  var dataHome = route.params;
  return (
    <View style={styles.container}>
      <ScrollView //component yang digunakan agar tampilan kita bisa discroll
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{paddingBottom: 10}}>
        <View style={{width: '100%'}}>
          <ImageBackground
            // source={require('../assets/image/image_seturan.png')} //load asset dari local
            source={{uri: dataHome.storeImage}}
            style={{
              width: '100%',
              height: 316,
            }}>
            <View
              style={{
                width: '100%',
                justifyContent: 'space-between',
                flexDirection: 'row',
                alignItems: 'center',
                marginVertical:10,
              }}>
              <TouchableOpacity
                onPress={() => navigation.goBack()}
                style={{
                  width: '10%',
                  justifyContent: 'center',
                  alignItems: 'center',
                  paddingVertical: 10,
                  paddingLeft:10,
                }}>
                <FontAwesome name="arrow-left" size={24} color="#FFFFFF" />
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => navigation.goBack()}
                style={{
                  width: '10%',
                  justifyContent: 'center',
                  alignItems: 'center',
                  paddingVertical: 10,
                  paddingRight:10,
                }}>
                <FontAwesome name="shopping-bag" size={24} color="#FFFFFF" />
              </TouchableOpacity>
            </View>
          </ImageBackground>
        </View>
        <View
          style={{
            paddingHorizontal: 20,
            paddingBottom: 34,
            paddingTop: 24,
            marginTop: -30,
            borderTopRightRadius: 20,
            borderTopLeftRadius: 20,
            backgroundColor: '#FFFFFF',
          }}>
          <View>
            <Text style={{fontSize: 18, fontWeight: '700', color: '#201F26'}}>
              {dataHome.storeName}
            </Text>
            <View style={styles.stars}>
              <Rating
                rating={dataHome.ratings}
                colorOn="yellow"
                iconSize={12}
              />
            </View>

            <View style={{flexDirection: 'row'}}>
              <Image
                source={require('../assets/icon/Location_ic.png')} //load asset dari local
                style={{
                  width: 14,
                  height: 18,
                  resizeMode: 'contain',
                  marginVertical: 5,
                  marginRight: 20,
                }}
              />
              <View style={styles.location}>
                <Text
                  style={{
                    fontSize: 10.5,
                    fontWeight: '400',
                    color: '#979797',
                    width: '60%',
                  }}>
                  {dataHome.address}
                </Text>
                <TouchableOpacity>
                  <Text
                    style={{
                      fontSize: 12.5,
                      fontWeight: '700',
                      color: '#3471CD',
                      right: -20,
                    }}>
                    Lihat Maps
                  </Text>
                </TouchableOpacity>
              </View>
            </View>

            <View style={styles.times}>
              <TouchableOpacity>
                <View
                  style={{
                    width: 55,
                    height: 21,
                    borderRadius: 10.5,
                    backgroundColor:
                      dataHome.isOpen == 'Buka'
                        ? 'rgba(17, 168, 78, 0.12)'
                        : 'rgba(230, 76, 60, 0.2)',
                    alignItems: 'center',
                    justifyContent: 'center',
                    marginVertical: 5,
                  }}>
                  <Text
                    style={{
                      fontSize: 12,
                      color: dataHome.isOpen == 'Buka' ? '#11A84E' : '#EA3D3D',
                      fontWeight: '700',
                    }}>
                    {dataHome.isOpen}
                  </Text>
                </View>
              </TouchableOpacity>
              <Text
                style={{
                  fontSize: 12,
                  color: '#343434',
                  fontWeight: '700',
                  marginLeft: 15,
                }}>
                {dataHome.availableTime} - {dataHome.closedTime}
              </Text>
            </View>
            <View
              style={{
                marginVertical: 10,
                backgroundColor: '#EEEEEE',
                width: '100%',
                height: 1,
              }}
            />
            <View style={styles.description}>
              <Text
                style={{
                  fontSize: 16,
                  color: '#201F26',
                  fontWeight: '500',
                  marginTop: 5,
                }}>
                Deskripsi
              </Text>
              <Text
                style={{
                  fontSize: 14,
                  color: '#595959',
                  fontWeight: '400',
                  marginVertical: 10,
                }}>
                {dataHome.description}
              </Text>
              <Text
                style={{
                  fontSize: 16,
                  color: '#201F26',
                  fontWeight: '500',
                }}>
                Range Biaya
              </Text>
              <Text
                style={{
                  fontSize: 16,
                  color: '#8D8D8D',
                  fontWeight: '500',
                  marginVertical: 5,
                }}>
                Rp
                {dataHome.minimumPrice} - {dataHome.maximumPrice}
              </Text>
            </View>
          </View>
        </View>
        <View
          style={{
            backgroundColor: '#FFFFFF',
            borderBottomLeftRadius: 20,
            borderBottomRightRadius: 20,
          }}>
          <View style={styles.cartButton}>
            <TouchableOpacity
              onPress={() => navigation.navigate('AddDataCart')}>
              <Text
                style={{
                  fontSize: 16.5,
                  color: '#FFFFFF',
                  fontWeight: '700',
                }}>
                Masukan Keranjang
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

export default DetailScreen;
