import React, { useState } from 'react';
import {
  View,
  Text,
  TextInput,
  ScrollView,
  Image,
  TouchableOpacity,
} from 'react-native';
import { styles } from './Style';
import { useDispatch, useSelector } from 'react-redux';
import moment from 'moment';

const AddDataCart = ({ navigation, route }) => {

  var dataStore = route.params
  console.log('dataStore', dataStore);

  const [merk, setMerk] = useState('')
  const [color, setColor] = useState('')
  const [size, setSize] = useState('')
  const [note, setNote] = useState('')

  const { cartData } = useSelector(state => state.cart);

  const dispatch = useDispatch();

  const storeData = () => {
    console.log('data', cartData);
    let dataCart = [...cartData];
    const data = {
      idCart: moment(new Date()).format('YYYYMMDDHHmmssSSS'),
      color: color,
      imageProduct: "https://assets.bwbx.io/images/users/iqjWHBFdfxIU/i4jPhKEFw1NE/v0/1200x-1.jpg",
      merk: merk,
      note: note,
      size: size,
      service: "ganti sepatu sandal",
    };
    dataCart.push(data);
    console.log('tambah data', dataCart);
    dispatch({ type: 'ADD_DATA_CART', data: dataCart });
    navigation.navigate('Keranjangs', dataCart);
  }

  const updateData = () => {
    //buat update data
    console.log('ubah', cartData);
    let dataCart = [...cartData];
    const data = {
      idCart: moment(new Date()).format('YYYYMMDDHHmmssSSS'),
      color: color,
      imageProduct: "https://assets.bwbx.io/images/users/iqjWHBFdfxIU/i4jPhKEFw1NE/v0/1200x-1.jpg",
      merk: merk,
      note: note,
      size: size,
      service: "ganti sepatu sandal",
    };
    dispatch({ type: 'UPDATE_DATA', data: dataCart });
    navigation.navigate('Keranjangs', dataCart);
  };

  const deleteData = async () => {
    dispatch({ type: 'DELETE_DATA', id: route.params.item.id });
    navigation.navigate('Keranjangs', dataCart);
  };

  const checkData = () => {
    //fungsi cek data
    if (route.params) {
      const data = route.params.item;
      setColor(data.color);
      setMerk(data.merk);
      setNote(data.note);
      setSize(data.size);
    }
  };

  React.useEffect(() => {
    checkData();
  }, []);

  return (
    <View style={{ flex: 1, backgroundColor: '#FFFFFF' }}>
      <View
        style={{
          width: '100%',
          height: 60,
          backgroundColor: '#FFFFFF',
          flexDirection: 'row',
          alignSelf: 'center',
          alignItems: 'center',
          paddingLeft: 14,
          shadowColor: '#000',
          shadowOffset: {
            width: 0,
            height: 1,
          },
          shadowOpacity: 0.22,
          shadowRadius: 2.22,

          elevation: 3,
        }}>
        <TouchableOpacity onPress={() => navigation.navigate('Home')}>
          <Image
            style={{
              size: 24,
              color: '#BB2427',
            }}
            source={require('../assets/icon/blackBack_ic.png')}
          />
        </TouchableOpacity>
        <Text
          style={{
            fontFamily: 'Montserrat',
            marginLeft: 14,
            fontWeight: '700',
            fontSize: 18,
            lineHeight: 17,
          }}>
          Formulir Pemesanan
        </Text>
      </View>
      <ScrollView //component yang digunakan agar tampilan kita bisa discroll
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{ paddingBottom: 10 }}>
        <View style={styles.orderForm}>
          <View style={{ marginBottom: 25 }}>
            <Text style={{ color: 'red', fontWeight: 'bold' }}>Merek</Text>
            <TextInput //component yang digunakan untuk memasukkan data yang kita inginkan
              placeholder="Masukan Merk Barang" //pada tampilan ini, kita ingin user memasukkan email
              onChangeText={text => setMerk(text)}
              style={{
                marginTop: 10,
                width: '100%',
                borderRadius: 8,
                backgroundColor: '#F6F8FF',
                paddingHorizontal: 10,
              }}
              keyboardType="default" //akan muncul tombol @ pada keyboard yang nanti akan memudahkan user mengisi email
            />
          </View>
          <View style={{ marginBottom: 25 }}>
            <Text style={{ color: 'red', fontWeight: 'bold' }}>Warna</Text>
            <TextInput //component yang digunakan untuk memasukkan data yang kita inginkan
              placeholder="Warna Barang, cth : Merah - Putih " //pada tampilan ini, kita ingin user memasukkan email
              onChangeText={text => setColor(text)}
              style={{
                marginTop: 10,
                width: '100%',
                borderRadius: 8,
                backgroundColor: '#F6F8FF',
                paddingHorizontal: 10,
              }}
              keyboardType="default" //akan muncul tombol @ pada keyboard yang nanti akan memudahkan user mengisi email
            />
          </View>
          <View style={{ marginBottom: 25 }}>
            <Text style={{ color: 'red', fontWeight: 'bold' }}>Ukuran</Text>
            <TextInput //component yang digunakan untuk memasukkan data yang kita inginkan
              placeholder="Cth : S, M, L / 39,40" //pada tampilan ini, kita ingin user memasukkan email
              onChangeText={text => setSize(text)}
              style={{
                marginTop: 10,
                width: '100%',
                borderRadius: 8,
                backgroundColor: '#F6F8FF',
                paddingHorizontal: 10,
              }}
              keyboardType="default" //akan muncul tombol @ pada keyboard yang nanti akan memudahkan user mengisi email
            />
          </View>
          <View style={{ marginBottom: 25 }}>
            <Text style={{ color: 'red', fontWeight: 'bold' }}>Photo</Text>
            <View style={styles.photoBox}>
              <Image
                source={require('../assets/icon/Camera_ic.png')} //load asset dari local
                style={{
                  width: 20,
                  height: 18,
                  marginBottom: 10,
                }}
              />
              <Text
                style={{
                  fontSize: 12,
                  color: '#BB2427',
                  fontWeight: '400',
                  marginTop: -1,
                }}>
                Add Photo
              </Text>
            </View>
            <View style={styles.checkBox}>
              <View
                style={{
                  borderWidth: 1,
                  borderRadius: 3,
                  borderColor: '#B8B8B8',
                  height: 24,
                  width: 24,
                }}></View>
              <Text
                style={{
                  fontSize: 14,
                  color: '#201F26',
                  fontWeight: '400',
                  marginLeft: 23,
                }}>
                Ganti Sol Sepatu
              </Text>
            </View>
            <View style={styles.checkBox}>
              <View
                style={{
                  borderWidth: 1,
                  borderRadius: 3,
                  borderColor: '#B8B8B8',
                  height: 24,
                  width: 24,
                }}></View>
              <Text
                style={{
                  fontSize: 14,
                  color: '#201F26',
                  fontWeight: '400',
                  marginLeft: 23,
                }}>
                Jahit Sepatu
              </Text>
            </View>
            <View style={styles.checkBox}>
              <View
                style={{
                  borderWidth: 1,
                  borderRadius: 3,
                  borderColor: '#B8B8B8',
                  height: 24,
                  width: 24,
                }}></View>
              <Text
                style={{
                  fontSize: 14,
                  color: '#201F26',
                  fontWeight: '400',
                  marginLeft: 23,
                }}>
                Repaint Sepatu
              </Text>
            </View>
            <View style={styles.checkBox}>
              <View
                style={{
                  borderWidth: 1,
                  borderRadius: 3,
                  borderColor: '#B8B8B8',
                  height: 24,
                  width: 24,
                }}></View>
              <Text
                style={{
                  fontSize: 14,
                  color: '#201F26',
                  fontWeight: '400',
                  marginLeft: 23,
                }}>
                Cuci Sepatu
              </Text>
            </View>
            <View style={styles.checkBox}>
              <View
                style={{
                  borderWidth: 1,
                  borderRadius: 3,
                  borderColor: '#B8B8B8',
                  height: 24,
                  width: 24,
                }}></View>
              <Text
                style={{
                  fontSize: 14,
                  color: '#201F26',
                  fontWeight: '400',
                  marginLeft: 23,
                }}>
                Ganti Sol Sepatu
              </Text>
            </View>
            <View style={{ marginBottom: 25, marginTop: 44 }}>
              <Text style={{ color: 'red', fontWeight: 'bold' }}>Catatan</Text>
              <TextInput //component yang digunakan untuk memasukkan data yang kita inginkan
                placeholder="Cth : ingin ganti sol baru" //pada tampilan ini, kita ingin user memasukkan email
                onChangeText={text => setNote(text)}
                style={{
                  marginTop: 10,
                  width: '100%',
                  height: 92,
                  textAlignVertical: 'top',
                  borderRadius: 8,
                  backgroundColor: '#F6F8FF',
                  paddingHorizontal: 10,
                }}
                keyboardType="default" //akan muncul tombol @ pada keyboard yang nanti akan memudahkan user mengisi email
              />
            </View>
            <View style={{ marginBottom: 25 }}>
              <Text style={{ color: 'red', fontWeight: 'bold' }}>Kupon Promo</Text>
              <View style={{
                marginTop: 10,
                width: '100%',
                height: 47,
                borderRadius: 8,
                borderColor: '#C03A2B',
                borderWidth: 1,
                paddingHorizontal: 10,
                flexDirection: 'row',
                justifyContent: 'flex-start',
                alignItems: 'center'
              }}>
                <Text>Pilih kupon Promo</Text>
                <Image
                  source={require('../assets/icon/arrow_ic.png')} //load asset dari local
                  style={{
                    marginLeft: 220,
                    width: 7,
                    height: 12,
                  }}
                />
              </View>
            </View>
          </View>
        </View>
        <View style={styles.cartButton}>
          <TouchableOpacity onPress={() => {
            if (route.params) {
              updateData();
            } else {
              storeData();
            }
          }}>
            <Text
              style={{
                fontSize: 16.5,
                color: '#FFFFFF',
                fontWeight: '700',
              }}>
              Masukan Keranjang
            </Text>
          </TouchableOpacity>
        </View>

        {route.params && (
          <View style={styles.cartButton}>
            <TouchableOpacity onPress={deleteData}
            >
              <Text
                style={{
                  fontSize: 16.5,
                  color: '#FFFFFF',
                  fontWeight: '700',
                }}>
                Hapus Data
              </Text>
            </TouchableOpacity>
          </View>
        )}

      </ScrollView>
    </View>
  );
};
export default AddDataCart;
