import React from 'react';
import {
  View,
  Text,
  TextInput,
  ScrollView,
  Image,
  TouchableOpacity,
} from 'react-native';
import {styles} from './Style';

const Transaction = ({navigation, route}) => {
  return (
    <View style={styles.container}>
      <View
        style={{
          width: '100%',
          height: 60,
          backgroundColor: '#FFFFFF',
          flexDirection: 'row',
          alignSelf: 'center',
          alignItems: 'center',
          paddingLeft: 14,
          shadowColor: '#000',
          shadowOffset: {
            width: 0,
            height: 1,
          },
          shadowOpacity: 0.22,
          shadowRadius: 2.22,

          elevation: 3,
        }}>
        <TouchableOpacity onPress={() => navigation.navigate('Home')}>
          <Image
            style={{
              size: 24,
              color: '#BB2427',
            }}
            source={require('../assets/icon/blackBack_ic.png')}
          />
        </TouchableOpacity>
        <Text
          style={{
            fontFamily: 'Montserrat',
            marginLeft: 14,
            fontWeight: '700',
            fontSize: 18,
            lineHeight: 17,
          }}>
          Transaksi
        </Text>
      </View>
      <ScrollView //component yang digunakan agar tampilan kita bisa discroll
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{paddingBottom: 10}}>
        <View
          style={{
            width: '100%',
            height: 128,
            backgroundColor: '#FFFFFF',
            alignSelf: 'center',
            flexDirection: 'column',
            backgroundColor: '#FFFFFF',
            marginVertical: 10,
            borderRadius:14,
          }}>
          <Text
            style={{
              paddingTop: 10,
              marginLeft: 14,
              fontFamily: 'Monsserrat',
              fontWeight: '500',
              fontSize: 14,
              lineHeight: 17,
              color: '#979797',
            }}>
            20 Desember 2020 - 09.00
          </Text>
          <Text
            style={{
              fontFamily: 'Monsserrat',
              fontWeight: '400',
              fontSize: 14,
              lineHeight: 17,
              color: '#000000',
              marginLeft: 14,
              paddingTop: 10,
            }}>
            New Balance - Pink Abu - 40
          </Text>
          <Text
            style={{
              fontFamily: 'Monsserrat',
              fontWeight: '400',
              fontSize: 14,
              lineHeight: 17,
              color: '#000000',
              marginLeft: 14,
              paddingTop: 6,
            }}>
            Cuci Sepatu
          </Text>
          <View style={{flexDirection:'row', justifyContent:'space-between'}}>
          <Text
            style={{
              fontFamily: 'Monsserrat',
              fontWeight: '400',
              fontSize: 14,
              lineHeight: 17,
              color: '#000000',
              marginLeft: 14,
              paddingTop: 6,
            }}>
            Kode Reservasi
            <Text style={{fontWeight: 'bold'}}> : CS2012321</Text>
          </Text>
          <TouchableOpacity style={{
            borderRadius:10.5,
            width:81,
            height:21,
            backgroundColor: 'rgba(242, 156, 31, 0.16)',
            alignItems:'center',
            justifyContent:'center',
            marginRight:7,

          }} onPress={() => navigation.navigate('Home')}>
            <Text
            style={{
                fontFamily: 'Monsserrat',
                fontWeight: '400',
                fontSize: 12,
                lineHeight: 17,
                color: '#FFC107',
              }}>Reserved</Text>
          </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};
export default Transaction;
