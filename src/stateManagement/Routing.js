import React from "react";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { NavigationContainer } from "@react-navigation/native";
import Login from "../screen/Login";
import Home from "../screen/Home";
import SplashScreen from "../screen/SplashScreen";
import AddDataHome from "../screen/AddDataHome";
import DetailScreen from "../screen/DetailScreen";
import ButtonNav from "../screen/ButtonNav";
import DatePickers from "../screen/DatePickers";
import AddDataCart from "../screen/AddDataCart";
import Keranjangs from "../screen/Keranjangs";
import EditProfile from "../screen/EditProfile";
import Faqs from "../screen/Faqs";
import Summary from "../screen/Summary";
import Checkout from "../screen/Checkout";
import Reservasi from "../screen/Reservasi";
import Register from "../screen/Register";



const Stack = createNativeStackNavigator();

export default function Routing() {
    return(
        <NavigationContainer>
            <Stack.Navigator screenOptions={{headerShown: false}}>
                <Stack.Screen name="SplashScreen" component={SplashScreen} />
                <Stack.Screen name="Login" component={Login} />
                <Stack.Screen name="Register" component={Register} />
                <Stack.Screen name="ButtonNav" component={ButtonNav} />
                <Stack.Screen name="AddDataHome" component={AddDataHome} />
                <Stack.Screen name="DetailScreen" component={DetailScreen} />
                <Stack.Screen name="AddDataCart" component={AddDataCart} />
                <Stack.Screen name="Keranjangs" component={Keranjangs} />
                <Stack.Screen name="DatePickers" component={DatePickers} />
                <Stack.Screen name="EditProfile" component={EditProfile} />
                <Stack.Screen name="Faqs" component={Faqs} />
                <Stack.Screen name="Summary" component={Summary} />
                <Stack.Screen name="Checkout" component={Checkout} />
                <Stack.Screen name="Reservasi" component={Reservasi} />
            </Stack.Navigator>
        </NavigationContainer>
    )
}