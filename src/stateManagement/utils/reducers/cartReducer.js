const initialState = {
    cartData: [],
  };
  
  const cartReducer = (state = initialState, action) => {
    switch (action.type) {
      case 'ADD_DATA_CART':
        return {
          ...state,
          cartData: action.data,
        };
        
      case 'UPDATE_DATA':
        var newData = [...state.cartData];
  
        var findIndex = state.cartData.findIndex((value) => {
          return value.id === action.data.id;
        });  
        newData[findIndex] = action.data;
        return {
          ...state,
          cartData: newData,
        };
  
      case 'DELETE_DATA':
          var newData = [...state.cartData];
  
        var findIndex = state.cartData.findIndex((value) => {
          return value.id === action.id;
        });
        newData.splice(findIndex, 1)
        return {
          ...state,
          cartData: newData,
        };
      default:
        return state;
    }
  };
  
  export default cartReducer;
  