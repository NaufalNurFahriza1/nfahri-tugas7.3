const initialState = {
    homeData: [],
  };
  
  const homeReducer = (state = initialState, action) => {
    switch (action.type) {
      case 'ADD_DATA':
        return {
          ...state,
          homeData: action.data,
        };
        
      case 'UPDATE_DATA':
        var newData = [...state.homeData];
  
        var findIndex = state.homeData.findIndex((value) => {
          return value.id === action.data.id;
        });
        newData[findIndex] = action.data;
        return {
          ...state,
          homeData: newData,
        };
  
      case 'DELETE_DATA':
          var newData = [...state.homeData];
  
        var findIndex = state.homeData.findIndex((value) => {
          return value.id === action.id;
        });
        newData.splice(findIndex, 1)
        return {
          ...state,
          homeData: newData,
        };
      default:
        return state;
    }
  };
  
  export default homeReducer;
  