import {combineReducers} from 'redux'
import authReducer from './reducers/authReducer'
import homeReducer from './reducers/homeReducer';
import cartReducer from './reducers/cartReducer';

const rootReducer = combineReducers({
   auth: authReducer,
   home: homeReducer,
   cart: cartReducer
})

export default rootReducer;
